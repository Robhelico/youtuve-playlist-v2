const express = require('express');
const yt = require('youtube-search-without-api-key');
const cors = require("cors");

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.options('*', cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post('/insert', async (req,res) => {
    console.log("beginning");
    console.log("Query received. About to check YouTube")
    const yt_search =  req.body;
    console.log(yt_search);
    const videos = await yt.search(yt_search.yt_search);
    res.status(200);
    res.send(videos[0]);
    console.log("end");

});

app.get('/', (req,res) => {
    res.status(200);
    res.send({"express": "YOUR EXPRESS BACKEND IS CONNECTED TO REACT"});
});

app.listen(port, () => {
    console.log("Backend endpoint listening at port " + port);
});